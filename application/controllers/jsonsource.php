<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jsonsource extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
        $jsonFile = file_get_contents("assets/codebeautyfy.json");
		$jsonData = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $jsonFile), true );
		$res = array();
		foreach($jsonData as $row) {
			$res[] = $row['Top Insurance Companies'];
		}
		echo json_encode($res);
    }
} // https://youtu.be/mzr61sLjGpg (12.47)
?>